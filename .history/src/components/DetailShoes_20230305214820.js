import React from 'react';

export default function DetailShoes(props) {
  let { detail } = props;
  return (
    <div>
      <h3>Detail Shoes</h3>
      <div className="row mt-5 p-5 text-left">
        <div className="col-3">
          <h5>{detail.name}</h5>
          <img className="img-fluid" src={detail.image} alt={detail.name} />
        </div>
        <div className="col-9">
          <h5>Product Specifications</h5>
          <p>Description: {detail.description}</p>
          <p>Price: {detail.price} $</p>
        </div>
      </div>
    </div>
  );
}
