import React, { useState } from 'react';
import { dataShoes } from './data_shoes';
import ListShoes from './ListShoes';
import DetailShoes from './DetailShoes';
import CartShoes from './CartShoes';

export default function ShoesShop() {
  let [stateListShoes, setStateListShoes] = useState({ listShoes: dataShoes });
  let [stateDetailShoes, setStateLDetailShoes] = useState({ detail: dataShoes[0] });
  let [stateCart, setStateCart] = useState({ cart: [] });

  let handleChangeDetail = (shoes) => {
    setStateLDetailShoes({ detail: shoes });
  };

  let handleAddToCart = (shoes) => {
    let cloneCart = [...setStateCart.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id === shoes.id;
    });
    if (index === -1) {
      let cartItem = { ...shoes, number: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[index].number++;
    }
    setStateCart({ cart: cloneCart });
  };

  return (
    <div>
      <h2>ShoesShop</h2>
      <CartShoes cart={stateCart.cart} />
      <ListShoes
        list={stateListShoes.listShoes}
        handleChangeDetail={handleChangeDetail}
        handleAddToCart={handleAddToCart}
      />
      <DetailShoes detail={stateDetailShoes.detail} />
    </div>
  );
}
