import React, { useState } from 'react';
import { dataShoes } from './data_shoes';
import ListShoes from './ListShoes';
import DetailShoes from './DetailShoes';
import CartShoes from './CartShoes';

export default function ShoesShop() {
  let [stateListShoes, setStateListShoes] = useState({ listShoes: dataShoes });
  let [stateDetailShoes, setStateLDetailShoes] = useState({ detail: dataShoes[0] });
  let [stateCart, setStateCart] = useState({ cart: [] });

  let handleChangeDetail = (shoes) => {
    setStateLDetailShoes({ detail: shoes });
  };

  return (
    <div>
      <h2>ShoesShop</h2>
      <CartShoes cart={cart} />
      <ListShoes list={stateListShoes.listShoes} handleChangeDetail={handleChangeDetail} />
      <DetailShoes detail={stateDetailShoes.detail} />
    </div>
  );
}
