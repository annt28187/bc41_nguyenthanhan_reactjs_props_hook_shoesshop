import React, { useState } from 'react';
import { dataShoes } from './data_shoes';
import ListShoes from './ListShoes';
import DetailShoes from './DetailShoes';

export default function ShoesShop() {
  let [stateListShoes, setStateListShoes] = useState({ listShoes: dataShoes });
  let [stateDetail, setStateLDetail] = useState({ detail: dataShoes[0] });

  return (
    <div>
      <h2>ShoesShop</h2>
      <ListShoes list={stateListShoes.listShoes} />
      <DetailShoes detail={stateDetail.detail} />
    </div>
  );
}
