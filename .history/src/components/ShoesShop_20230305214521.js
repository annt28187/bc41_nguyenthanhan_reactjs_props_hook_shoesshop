import React, { useState } from 'react';
import { dataShoes } from './data_shoes';
import ListShoes from './ListShoes';
import DetailShoes from './DetailShoes';

export default function ShoesShop() {
  let [stateListShoes, setStateListShoes] = useState({ listShoes: dataShoes });
  let [stateDetailShoes, setStateLDetailShoes] = useState({ detail: dataShoes[0] });

  let handleChangeDetail = (shoes) => {
    stateDetail;
  };

  return (
    <div>
      <h2>ShoesShop</h2>
      <ListShoes list={stateListShoes.listShoes} />
      <DetailShoes detail={stateDetailShoes.detail} />
    </div>
  );
}
