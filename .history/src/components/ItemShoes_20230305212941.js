import React from 'react';

export default function ItemShoes(shoes) {
  let [image, name, price] = shoes;
  return (
    <div className="col-4 p-4">
      <div className="card border-primary h-100">
        <img className="card-img-top" src={image} alt={name} />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">{price} $</p>
          <button className="btn btn-primary mr-2">Add to cart</button>
          <button className="btn btn-secondary">Show Detail</button>
        </div>
      </div>
    </div>
  );
}
