import React from 'react';
import ItemShoes from './ItemShoes';

export default function ListShoes(props) {
  let { list } = props;
  let renderListShoes = () => {
    return list.map((item, index) => {
      return <ItemShoes key={index} shoes={item} />;
    });
  };
  return <div className="row">{renderListShoes()}</div>;
}
