import React, { useState } from 'react';
import { dataShoes } from './data_shoes';
import ListShoes from './ListShoes';
import DetailShoes from './DetailShoes';
import CartShoes from './CartShoes';

export default function ShoesShop() {
  let [stateListShoes] = useState({ listShoes: dataShoes });
  let [stateDetailShoes, setStateLDetailShoes] = useState({ detail: dataShoes[0] });
  let [stateCart, setStateCart] = useState({ cart: [] });

  let handleChangeDetail = (shoes) => {
    setStateLDetailShoes({ detail: shoes });
  };

  let handleAddToCart = (shoes) => {
    let cloneCart = [...stateCart.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id === shoes.id;
    });
    if (index === -1) {
      let cartItem = { ...shoes, number: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[index].number++;
    }
    setStateCart({ cart: cloneCart });
  };

  let handleChangeQuantity = (idShoes, choose) => {
    let cloneCart = [...stateCart.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id === idShoes;
    });
    if (index === -1) {
      cloneCart[index].number = cloneCart[index].number + choose;
    }
    cloneCart[index].number === 0 && cloneCart.splice(index, 1);
    setStateCart({ cart: cloneCart });
  };

  let handleDeleteToCart = (idShoes) => {
    let newCart = stateCart.cart.filter((item) => {
      return item.id !== idShoes;
    });
    setStateCart({ cart: newCart });
  };

  return (
    <div>
      <h2>ShoesShop</h2>
      <CartShoes cart={stateCart.cart} />
      <ListShoes
        list={stateListShoes.listShoes}
        handleChangeDetail={handleChangeDetail}
        handleAddToCart={handleAddToCart}
      />
      <DetailShoes detail={stateDetailShoes.detail} />
    </div>
  );
}
