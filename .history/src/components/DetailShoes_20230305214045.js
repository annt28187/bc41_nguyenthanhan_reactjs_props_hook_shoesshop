import React from 'react';

export default function DetailShoes() {
  return (
    <div>
      <h3>Detail Shoes</h3>
      <div className="row mt-5 p-5 text-left">
        <div className="col-3">
          <h5>{name}</h5>
          <img className="img-fluid" src={image} alt={name} />
        </div>
        <div className="col-9">
          <h5>Product Specifications</h5>
          <p>Description: {description}</p>
          <p>Price: {price} $</p>
        </div>
      </div>
    </div>
  );
}
