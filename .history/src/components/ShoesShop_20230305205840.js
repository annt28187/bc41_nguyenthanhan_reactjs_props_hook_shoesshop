import React, { useState } from 'react';
import { dataShoes } from './data_shoes';
import ListShoes from './ListShoes';

export default function ShoesShop() {
  let [dataShoes] = useState([]);

  return (
    <div>
      <h2>ShoesShop</h2>
      <ListShoes list={dataShoes} />
    </div>
  );
}
