import React from 'react';

export default function CartShoes(props) {
  let { cart, handleChangeQuantity, handleDeleteToCart } = props;
  let renderTbody = () => {
    return cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img src={item.image} style={{ width: 50 }} alt={item.name} />
          </td>
          <td>
            <button
              className="btn btn-primary"
              onClick={() => {
                handleChangeQuantity(item.id, -1);
              }}
            >
              -
            </button>
            <strong className="mx-2">{item.number}</strong>
            <button className="btn btn-primary">+</button>
          </td>
          <td>{item.price}$</td>
          <td>{item.price * item.number}$</td>
          <td>
            <button className="btn btn-warning">Delete</button>
          </td>
        </tr>
      );
    });
  };
  return (
    <div>
      <table className="table">
        <thead>
          <th>ID</th>
          <th>Name</th>
          <th>Image</th>
          <th>Quantity</th>
          <th>Price</th>
          <th>Into money</th>
          <th>Action</th>
        </thead>
        <tbody>{renderTbody()}</tbody>
      </table>
    </div>
  );
}
