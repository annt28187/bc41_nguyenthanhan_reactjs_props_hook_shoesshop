import React from 'react';

export default function ItemShoes(props) {
  let { shoes, handleDetailClick, handleCartClick } = props;
  return (
    <div className="col-4 p-4">
      <div className="card border-primary h-100">
        <img className="card-img-top" src={shoes.image} alt={shoes.name} />
        <div className="card-body">
          <h5 className="card-title">{shoes.name}</h5>
          <p className="card-text">{shoes.price} $</p>
          <button
            className="btn btn-primary mr-2"
            onClick={() => {
              handleCartClick(shoes);
            }}
          >
            Add to cart
          </button>
          <button
            className="btn btn-secondary"
            onClick={() => {
              handleDetailClick(shoes);
            }}
          >
            Show Detail
          </button>
        </div>
      </div>
    </div>
  );
}
